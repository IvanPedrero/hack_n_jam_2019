﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavMeshAgentCreator : MonoBehaviour{

	public GameObject[] agentPrefab;
	public int agents = 10;
	public float rangeX;
	public float rangeY;
	public Color gizmoColor;
	public bool drawGizmo;

    void Start(){
    	for(int i = 0; i < agents; i++){
    		Vector3 targetPosition = transform.position;
    		targetPosition.x += Random.Range(-rangeX, rangeX);
    		targetPosition.z += Random.Range(-rangeY, rangeY);
    		Instantiate(agentPrefab[Random.Range(0,agentPrefab.Length)], targetPosition, Quaternion.identity);
    	}
        
    }

    void Update(){
        
    }

    void OnDrawGizmosSelected(){
    	if(drawGizmo){
    		Gizmos.DrawWireCube(transform.position, new Vector3(rangeX*2 ,0,rangeY*2));
    	}
    }
}
