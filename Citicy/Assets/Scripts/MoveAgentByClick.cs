﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum AgentType{human, car, bike, pubus};

public class MoveAgentByClick : MonoBehaviour{

	Camera cam;
	NavMeshAgent agent;
	public float speed = 0.5f;

    public GameObject[] hotspots;

    public AgentType agentType;

	void Awake(){
		cam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
		agent = GetComponent<NavMeshAgent>();
        hotspots = GameObject.FindGameObjectsWithTag("HS");
	}

    void Start(){
        ChangeHotspot();
        if(agentType == AgentType.human){
            speed = Random.Range(0.5f,1.2f);
            agent.speed = speed;
        }
    }

    void Update(){
        if(Input.GetMouseButtonDown(0)){
        	Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        	RaycastHit hit;
        	if(Physics.Raycast(ray, out hit)){
        		agent.SetDestination(hit.point);
        	}
        }
    }

    void ChangeHotspot(){
        agent.SetDestination(hotspots[Random.Range(0,hotspots.Length)].transform.position);
        Invoke("ChangeHotspot",Random.Range(5,7));
    }
}
