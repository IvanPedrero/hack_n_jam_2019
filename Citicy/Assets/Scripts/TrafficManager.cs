﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficManager : MonoBehaviour
{
    [Header("Diego is going to access this variable :")]
    [Range(0,10)]
    public int actualTraffic;

    //public string[] items;

/*
    */

    void Start(){
    	Invoke("GenerateRandomValue",5.0f);
    	//RetrieveDataFromServer();
    }

    void GenerateRandomValue(){
    	actualTraffic = UnityEngine.Random.Range(0,10);
    	Invoke("GenerateRandomValue",30.0f);
    }

    void RetrieveDataFromServer(){
    	StartCoroutine("ServerPull");
    	Invoke("RetrieveDataFromServer",30.0f);
    }

    IEnumerator ServerPull(){
        WWW itemsData = new WWW("http://192.185.131.34/citicy/connection.php");
        yield return itemsData;
        string itemsDataString = itemsData.text;
        //print(itemsDataString);
        Debug.Log("Data string: " + itemsDataString);
        //actualTraffic = int.Parse(itemsDataString);
        if(!itemsDataString.Equals("")){
        	actualTraffic = Int32.Parse(itemsDataString);
        }else{
        	actualTraffic = 0;
        }
    }
    /*string GetDataValue(string data, string index)
    {
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if (value.Contains("|")) value = value.Remove(value.IndexOf("|"));
        return value;
    }*/

}
