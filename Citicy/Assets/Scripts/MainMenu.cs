﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : Menu{

	public string[] sugerencias;
	public Text suggestionText;
	public GameObject watsonMessage;
	public GameObject myMessage;
	public GameObject chatContent;
	public RectTransform rt;
	public float messageSize = 80;
	public InputField userInput;
	int messagesSent;

	void Awake(){
		rt = chatContent.GetComponent<RectTransform>();
	}

    void Start(){
        base.Start();
        messagesSent = 0;
    }

    // Update is called once per frame
    void Update(){
        if(Input.GetKeyDown(KeyCode.Return) && !userInput.text.Equals("")){
        	SendUserMessage();
        }
    }

    public void GetNewSuggestion(){
    	suggestionText.text = sugerencias[Random.Range(0,sugerencias.Length)];
    }

    public void SendUserMessage(){
    	//Get the message
    	string messagetoSend = userInput.text;
    	if(!messagetoSend.Equals("")){
	    	GameObject newMessage = Instantiate(myMessage, Vector3.zero, Quaternion.identity, chatContent.transform) as GameObject;
	    	newMessage.transform.GetChild(0).GetComponent<Text>().text = "  " + messagetoSend;
	    	userInput.text = "";
	    	//Change the size
	    	messagesSent++;
	    	Vector2 newSizeDelta = rt.sizeDelta;
	    	Debug.Log("New target size: " + messagesSent * messageSize);
	    	newSizeDelta.y = messagesSent * messageSize;
	    	rt.sizeDelta = newSizeDelta;
    	}
    }

    void SendWatsonMessage(string ms){
		GameObject newMessage = Instantiate(watsonMessage, Vector3.zero, Quaternion.identity, chatContent.transform) as GameObject;
    	newMessage.transform.GetChild(0).GetComponent<Text>().text = "  " + ms;
    	//Change the size
    	messagesSent++;
	    //Debug.Log("New target size: " + messagesSent * messageSize);
    	Vector2 newSizeDelta = rt.sizeDelta;
    	newSizeDelta.y = messagesSent * messageSize;
    	//Debug.Log("NSD : " + newSizeDelta.y);
    	rt.sizeDelta = newSizeDelta;
    }


    public void StartChatson(){
    	Invoke("Message1", 1.0f);
    	Invoke("Message2", 2.0f);
    }

    void Message1(){
    	SendWatsonMessage("¡Hola!, Mi nombre es Watson Cognitive Services, como podemos ayudarte");
    }
    void Message2(){
    	SendWatsonMessage("¿Como podemos ayudarte?");
    }
}
