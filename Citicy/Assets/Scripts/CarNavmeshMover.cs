﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CarNavmeshMover : MonoBehaviour {

	Transform[] waypoints;
	public Transform route;
	Transform currentWaypoint;
	float distanceToTarget;
	int waypointIndex;
	NavMeshAgent agent;
	public GameObject[] wheels;
	Vector3 previousPosition;
	Vector3 rotationVector;

	public float movementSpeed = 20.0f;
	float speedMultiplier = 1.0f;
	float speed;

	void Start () {
		waypoints = new Transform[route.childCount];
		for(int i = 0; i < route.childCount; i++){
			waypoints[i] = route.GetChild(i);
		}
		ChooseTargetByDistance();
		agent = GetComponent<NavMeshAgent>();
		agent.destination = currentWaypoint.transform.position;
		rotationVector = Vector3.zero;
		
	}
	void Update () {
		//Debug.Log("Distance to target: " + Vector3.Distance(transform.position,currentWaypoint.position));
		//transform.LookAt(agent.destination);
		distanceToTarget = Vector3.Distance(transform.position, currentWaypoint.position);
		if(distanceToTarget <= 8.0f){
			speedMultiplier =  0.3f;
		}else{
			speedMultiplier = 1.0f;
		}
		agent.speed = movementSpeed * speedMultiplier;
		if(distanceToTarget <= 3.0f){
			currentWaypoint = waypoints[waypointIndex++ % waypoints.Length];
			Vector3 targetPosition = currentWaypoint.transform.position;
			targetPosition.y = transform.position.y;
			agent.destination = targetPosition;
		}

		speed = (Vector3.Distance (transform.position, previousPosition))/Time.deltaTime;
		//Debug.Log("Speed: " + speed);
		previousPosition = transform.position;
		rotationVector.x += speed;
		for(int i = 0; i < wheels.Length; i++){
			wheels[i].transform.localEulerAngles = rotationVector;
		}
	}

	void ChooseTargetByDistance(){
		currentWaypoint = waypoints[0];
		waypointIndex = 0;
		for(int i = 1; i < waypoints.Length-1; i++){
			if(Vector3.Distance(transform.position, waypoints[i].transform.position) < Vector3.Distance(transform.position, currentWaypoint.position)){
				currentWaypoint = waypoints[i];
				waypointIndex = i;
			}
		}
		Debug.Log("Choose: " + currentWaypoint.transform.name);
	}
	
}
