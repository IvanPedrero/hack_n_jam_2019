﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdatePartSystem : MonoBehaviour{
	

	public Color green;
	public Color red;
	ParticleSystem ps;
	public GameObject trafficManager;
	TrafficManager tm;
	public Color lerpedColor;
	public float updateInterval = 30.0f;

	void Awake(){
		ps = GetComponent<ParticleSystem>();
		tm = trafficManager.GetComponent<TrafficManager>();
	}

    void Start(){
        UpdateStartColor();
    }

    // Update is called once per frame
    void Update(){
        //lerpedColor = Color.Lerp(green, red, Mathf.PingPong(Time.time, 1));
        //Debug.Log("PP :"+ Mathf.PingPong(Time.time, 1));
    	lerpedColor = Color.Lerp(green,red,((float)tm.actualTraffic)/10.0f);
        ps.startColor = lerpedColor;
    }

    void UpdateStartColor(){
    	lerpedColor = Color.Lerp(green,red,Mathf.Clamp(tm.actualTraffic/10,0,1));
    	Invoke("UpdateStartColor",updateInterval);
        ps.startColor = lerpedColor;
    }
}
